#!/vendor/bin/sh
#
function oplus_configure_zram_parameters() {
	#huacai.zhou@PSW.BSP.kernel.drv, 2018/03/09, adjust zram size according to total ram size
	MemTotalStr=`cat /proc/meminfo | grep MemTotal`
	MemTotal=${MemTotalStr:16:8}

	echo lz4 > /sys/block/zram0/comp_algorithm
	echo 160 > /proc/sys/vm/swappiness
	echo 60 > /sys/module/oplus_bsp_zram_opt/parameters/direct_vm_swappiness
	echo 0 > /proc/sys/vm/page-cluster
	if [ -f /sys/block/zram0/disksize ]; then
		if [ -f /sys/block/zram0/use_dedup ]; then
			echo 1 > /sys/block/zram0/use_dedup
		fi

		if [ $MemTotal -le 524288 ]; then
			#config 384MB zramsize with ramsize 512MB
			echo 402653184 > /sys/block/zram0/disksize
		elif [ $MemTotal -le 1048576 ]; then
			#config 768MB zramsize with ramsize 1GB
			echo 805306368 > /sys/block/zram0/disksize
		elif [ $MemTotal -le 2097152 ]; then
			#config 1GB+256MB zramsize with ramsize 2GB
			echo lz4 > /sys/block/zram0/comp_algorithm
			echo 1342177280 > /sys/block/zram0/disksize
		elif [ $MemTotal -le 3145728 ]; then
			#config 1GB+512MB zramsize with ramsize 3GB
			echo lz4 > /sys/block/zram0/comp_algorithm
			echo 1610612736 > /sys/block/zram0/disksize
		elif [ $MemTotal -le 4194304 ]; then
			#config 2GB+512MB zramsize with ramsize 4GB
			echo 2684354560 > /sys/block/zram0/disksize
		elif [ $MemTotal -le 6291456 ]; then
			#config 3GB zramsize with ramsize 6GB
			echo 3221225472 > /sys/block/zram0/disksize
		else
			#config 4GB zramsize with ramsize >=8GB
			echo 4294967296 > /sys/block/zram0/disksize
		fi
		/vendor/bin/mkswap /dev/block/zram0
		/vendor/bin/swapon /dev/block/zram0
	fi
}

function configure_para_even()
{
	product_id=`getprop ro.boot.prjname`
	case "$product_id" in
		"2167A"|"2167B"|"2167C"|"2167D"|"216AF"|"216B0"|"216B1")
			oplus_configure_zram_parameters
	esac
}

function configure_memory_parameters() {
	# For vts test which has replace system.img
	ls -l /product | grep '\-\>'
	if [ $? -eq 0 ]; then
		oplus_configure_zram_parameters
	fi
	configure_para_even
}

configure_memory_parameters
